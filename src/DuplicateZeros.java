import java.util.Scanner;

public class DuplicateZeros {

    static void duplicateZero(int[] arr){
            for (int i = arr.length - 1; i >= 0; i--) {
                if (arr[i] == 0) {
                    for (int j = arr.length - 1; j > i; j--) {
                        arr[j] = arr[j-1];
                    }
                }
            }
    }

    static void printArray(int[] arr){
        System.out.print("[");
        for(int i=0; i<arr.length; i++){
            if(i==arr.length-1){
                System.out.print(arr[i]);
            }else{
                System.out.print(arr[i]+",");
            }
        }
        System.out.println("]");
    }

    public static void main(String[] args) {

        int[] arr = {1,0,2,3,0,4,5,0};
        System.out.print("arr = ");
        printArray(arr);
        duplicateZero(arr);
        printArray(arr);
    }
}

